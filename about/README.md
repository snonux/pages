The paul.buetow.org internet site
=================================

This repository contains the static files of my internet site.  [gemini://paul.buetow.org](gemini://paul.buetow.org) and [https://paul.buetow.org](https://paul.buetow.org).

Each format is in it's own branch in this repository. E.g.:

* Gemtext is in `content-gemtext`
* HTML is in `content-html`
* Markdown is in `content-md`
* ... ando so on.

You can find more about my internet site and the static content generator at [snonux/gemtexter](https://codeberg.org/snonux/gemtexter).
