<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DTail - The distributed log tail program</title>
<link rel="shortcut icon" type="image/gif" href="/favicon.ico" />
<link rel="stylesheet" href="../style.css" />
<link rel="stylesheet" href="style-override.css" />
</head>
<body>
<p class="header">
<a href="https://foo.zone">Home</a> | <a href="https://codeberg.org/snonux/foo.zone/src/branch/content-md/gemfeed/2021-04-22-dtail-the-distributed-log-tail-program.md">Markdown</a> | <a href="gemini://foo.zone/gemfeed/2021-04-22-dtail-the-distributed-log-tail-program.gmi">Gemini</a>
</p>
<h1 style='display: inline' id='dtail---the-distributed-log-tail-program'>DTail - The distributed log tail program</h1><br />
<br />
<span class='quote'>Published at 2021-04-22T19:28:41+01:00; Updated at 2021-04-26</span><br />
<br />
<span>This article first appeared at the Mimecast Engineering Blog but I made it available here in my personal internet site too.</span><br />
<br />
<a class='textlink' href='https://medium.com/mimecast-engineering/dtail-the-distributed-log-tail-program-79b8087904bb'>Original Mimecast Engineering Blog post at Medium</a><br />
<br />
<span>Running a large cloud-based service requires monitoring the state of huge numbers of machines, a task for which many standard UNIX tools were not really designed. In this post, I will describe a simple program, DTail, that Mimecast has built and released as Open-Source, which enables us to monitor log files of many servers at once without the costly overhead of a full-blown log management system.</span><br />
<br />
<span>At Mimecast, we run over 10 thousand server boxes. Most of them host multiple microservices and each of them produces log files. Even with the use of time series databases and monitoring systems, raw application logs are still an important source of information when it comes to analysing, debugging, and troubleshooting services.</span><br />
<br />
<span>Every engineer familiar with UNIX or a UNIX-like platform (e.g., Linux) is well aware of tail, a command-line program for displaying a text file content on the terminal which is also especially useful for following application or system log files with tail -f logfile.</span><br />
<br />
<span>Think of DTail as a distributed version of the tail program which is very useful when you have a distributed application running on many servers. DTail is an Open-Source, cross-platform, fairly easy to use, support and maintain log file analysis &amp; statistics gathering tool designed for Engineers and Systems Administrators. It is programmed in Google Go.</span><br />
<br />
<a href='./dtail-the-distributed-log-tail-program/title.png'><img alt='DTail logo image' title='DTail logo image' src='./dtail-the-distributed-log-tail-program/title.png' /></a><br />
<br />
<h2 style='display: inline' id='table-of-contents'>Table of Contents</h2><br />
<br />
<ul>
<li><a href='#dtail---the-distributed-log-tail-program'>DTail - The distributed log tail program</a></li>
<li>⇢ <a href='#a-mimecast-pet-project'>A Mimecast Pet Project</a></li>
<li>⇢ <a href='#differentiating-from-log-management-systems'>Differentiating from log management systems</a></li>
<li>⇢ <a href='#combining-simplicity-security-and-efficiency'>Combining simplicity, security and efficiency</a></li>
<li>⇢ <a href='#the-dtail-family-of-commands'>The DTail family of commands</a></li>
<li>⇢ <a href='#usage-example'>Usage example</a></li>
<li>⇢ <a href='#fitting-it-in'>Fitting it in</a></li>
<li>⇢ <a href='#advanced-features'>Advanced features</a></li>
<li>⇢ <a href='#for-the-future'>For the future</a></li>
<li>⇢ <a href='#open-source'>Open Source</a></li>
</ul><br />
<h2 style='display: inline' id='a-mimecast-pet-project'>A Mimecast Pet Project</h2><br />
<br />
<span>DTail got its inspiration from public domain tools available already in this area but it is a blue sky from-scratch development which was first presented at Mimecast’s annual internal Pet Project competition (awarded with a Bronze prize). It has gained popularity since and is one of the most widely deployed DevOps tools at Mimecast (reaching nearly 10k server installations) and many engineers use it on a regular basis. The Open-Source version of DTail is available at:</span><br />
<br />
<a class='textlink' href='https://dtail.dev'>https://dtail.dev</a><br />
<br />
<span>Try it out — We would love any feedback. But first, read on…</span><br />
<br />
<h2 style='display: inline' id='differentiating-from-log-management-systems'>Differentiating from log management systems</h2><br />
<br />
<span>Why not just use a full-blown log management system? There are various Open-Source and commercial log management solutions available on the market you could choose from (e.g. the ELK stack). Most of them store the logs in a centralized location and are fairly complex to set up and operate. Possibly they are also pretty expensive to operate if you have to buy dedicated hardware (or pay fees to your cloud provider) and have to hire support staff for it.</span><br />
<br />
<span>DTail does not aim to replace any of the log management tools already available but is rather an additional tool crafted especially for ad-hoc debugging and troubleshooting purposes. DTail is cheap to operate as it does not require any dedicated hardware for log storage as it operates directly on the source of the logs. It means that there is a DTail server installed on all server boxes producing logs. This decentralized comes with the direct advantages that there is no introduced delay because the logs are not shipped to a central log storage device. The reduced complexity also makes it more robust against outages. You won’t be able to troubleshoot your distributed application very well if the log management infrastructure isn’t working either.</span><br />
<br />
<a href='./dtail-the-distributed-log-tail-program/dtail.gif'><img alt='DTail sample session animated gif' title='DTail sample session animated gif' src='./dtail-the-distributed-log-tail-program/dtail.gif' /></a><br />
<br />
<span>As a downside, you won’t be able to access any logs with DTail when the server is down. Furthermore, a server can store logs only up to a certain capacity as disks will fill up. For the purpose of ad-hoc debugging, these are not typically issues. Usually, it’s the application you want to debug and not the server. And disk space is rarely an issue for bare metal and VM-based systems these days, with sufficient space for several weeks’ worth of log storage being available. DTail also supports reading compressed logs. The currently supported compression algorithms are gzip and zstd.</span><br />
<br />
<h2 style='display: inline' id='combining-simplicity-security-and-efficiency'>Combining simplicity, security and efficiency</h2><br />
<br />
<span>DTail also has a client component that connects to multiple servers concurrently for log files (or any other text files).</span><br />
<br />
<span>The DTail client interacts with a DTail server on port TCP/2222 via SSH protocol and does not interact in any way with the system’s SSH server (e.g., OpenSSH Server) which might be running at port TCP/22 already. As a matter of fact, you don’t need a regular SSH server running for DTail at all. There is no support for interactive login shells at TCP/2222 either, as by design that port can only be used for text data streaming. The SSH protocol is used for the public/private key infrastructure and transport encryption only and DTail implements its own protocol on top of SSH for the features provided. There is no need to set up or buy any additional TLS certificates. The port 2222 can be easily reconfigured if you preferred to use a different one.</span><br />
<br />
<span>The DTail server, which is a single static binary, will not fork an external process. This means that all features are implemented in native Go code (exception: Linux ACL support is implemented in C, but it must be enabled explicitly on compile time) and therefore helping to make it robust, secure, efficient, and easy to deploy. A single client, running on a standard Laptop, can connect to thousands of servers concurrently while still maintaining a small resource footprint.</span><br />
<br />
<span>Recent log files are very likely still in the file system caches on the servers. Therefore, there tends to be a minimal I/O overhead involved.</span><br />
<br />
<h2 style='display: inline' id='the-dtail-family-of-commands'>The DTail family of commands</h2><br />
<br />
<span>Following the UNIX philosophy, DTail includes multiple command-line commands each of them for a different purpose:</span><br />
<br />
<ul>
<li>dserver: The DTail server, the only binary required to be installed on the servers involved.</li>
<li>dtail: The distributed log tail client for following log files.</li>
<li>dcat: The distributed cat client for concatenating and displaying text files.</li>
<li>dgrep: The distributed grep client for searching text files for a regular expression pattern.</li>
<li>dmap: The distributed map-reduce client for aggregating stats from log files.</li>
</ul><br />
<a href='./dtail-the-distributed-log-tail-program/dgrep.gif'><img alt='DGrep sample session animated gif' title='DGrep sample session animated gif' src='./dtail-the-distributed-log-tail-program/dgrep.gif' /></a><br />
<br />
<h2 style='display: inline' id='usage-example'>Usage example</h2><br />
<br />
<span>The use of these commands is almost self-explanatory for a person already used to the standard command line in Unix systems. One of the main goals is to make DTail easy to use. A tool that is too complicated to use under high-pressure scenarios (e.g., during an incident) can be quite detrimental.</span><br />
<br />
<span>The basic idea is to start one of the clients from the command line and provide a list of servers to connect to with –servers. You also must provide a path of remote (log) files via –files. If you want to process multiple files per server, you could either provide a comma-separated list of file paths or make use of file system globbing (or a combination of both).</span><br />
<br />
<span>The following example would connect to all DTail servers listed in the serverlist.txt, follow all files with the ending .log and filter for lines containing the string error. You can specify any Go compatible regular expression. In this example we add the case-insensitive flag to the regex:</span><br />
<br />
<pre>
dtail –servers serverlist.txt –files ‘/var/log/*.log’ –regex ‘(?i:error)’
</pre>
<br />
<span>You usually want to specify a regular expression as a client argument. This will mean that responses are pre-filtered for all matching lines on the server-side and thus sending back only the relevant lines to the client. If your logs are growing very rapidly and the regex is not specific enough there might be the chance that your client is not fast enough to keep up processing all of the responses. This could be due to a network bottleneck or just as simple as a slow terminal emulator displaying the log lines on the client-side.</span><br />
<br />
<span>A green 100 in the client output before each log line received from the server always indicates that there were no such problems and 100% of all log lines could be displayed on your terminal (have a look at the animated Gifs in this post). If the percentage falls below 100 it means that some of the channels used by the servers to send data to the client are congested and lines were dropped. In this case, the color will change from green to red. The user then could decide to run the same query but with a more specific regex.</span><br />
<br />
<span>You could also provide a comma-separated list of servers as opposed to a text file. There are many more options you could use. The ones listed here are just the very basic ones. There are more instructions and usage examples on the GitHub page. Also, you can study even more of the available options via the –help switch (some real treasures might be hidden there).</span><br />
<br />
<h2 style='display: inline' id='fitting-it-in'>Fitting it in</h2><br />
<br />
<span>DTail integrates nicely into the user management of existing infrastructure. It follows normal system permissions and does not open new “holes” on the server which helps to keep security departments happy. The user would not have more or less file read permissions than he would have via a regular SSH login shell. There is a full SSH key, traditional UNIX permissions, and Linux ACL support. There is also a very low resource footprint involved. On average for tailing and searching log files less than 100MB RAM and less than a quarter of a CPU core per participating server are required. Complex map-reduce queries on big data sets will require more resources accordingly.</span><br />
<br />
<h2 style='display: inline' id='advanced-features'>Advanced features</h2><br />
<br />
<span>The features listed here are out of the scope of this blog post but are worthwhile to mention:</span><br />
<br />
<ul>
<li>Distributed map-reduce queries on stats provided in log files with dmap. dmap comes with its own SQL-like aggregation query language.</li>
<li>Stats streaming with continuous map-reduce queries. The difference to normal queries is that the stats are aggregated over a specified interval only on the newly written log lines. Thus, giving a de-facto live stat view for each interval.</li>
<li>Server-side scheduled queries on log files. The queries are configured in the DTail server configuration file and scheduled at certain time intervals. Results are written to CSV files. This is useful for generating daily stats from the log files without the need for an interactive client.</li>
<li>Server-side stats streaming with continuous map-reduce queries. This for example can be used to periodically generate stats from the logs at a configured interval, e.g., log error counts by the minute. These then can be sent to a time-series database (e.g., Graphite) and then plotted in a Grafana dashboard.</li>
<li>Support for custom extensions. E.g., for different server discovery methods (so you don’t have to rely on plain server lists) and log file formats (so that map-reduce queries can parse more stats from the logs).</li>
</ul><br />
<h2 style='display: inline' id='for-the-future'>For the future</h2><br />
<br />
<span>There are various features we want to see in the future.</span><br />
<br />
<ul>
<li>A spartan mode, not printing out any extra information but the raw remote log files would be a nice feature to have. This will make it easier to post-process the data produced by the DTail client with common UNIX tools. (To some degree this is possible already, just disable the ANSI terminal color output of the client with -noColors and pipe the output to another program).</li>
<li>Tempting would be implementing the dgoawk command, a distributed version of the AWK programming language purely implemented in Go, for advanced text data stream processing capabilities. There are 3rd party libraries available implementing AWK in pure Go which could be used.</li>
<li>A more complex change would be the support of federated queries. You can connect to thousands of servers from a single client running on a laptop. But does it scale to 100k of servers? Some of the servers could be used as middleware for connecting to even more servers.</li>
<li>Another aspect is to extend the documentation. Especially the advanced features such as map-reduce query language and how to configure the server-side queries currently do require more documentation. For now, you can read the code, sample config files or just ask the author for that! But this will be certainly addressed in the future.</li>
</ul><br />
<h2 style='display: inline' id='open-source'>Open Source</h2><br />
<br />
<span>Mimecast highly encourages you to have a look at DTail and submit an issue for any features you would like to see. Have you found a bug? Maybe you just have a question or comment? If you want to go a step further: We would also love to see pull requests for any features or improvements. Either way, if in doubt just contact us via the DTail GitHub page.</span><br />
<br />
<a class='textlink' href='https://dtail.dev'>https://dtail.dev</a><br />
<br />
<span>E-Mail your comments to <span class='inlinecode'>paul@nospam.buetow.org</span> :-)</span><br />
<br />
<span>Other related posts are:</span><br />
<br />
<a class='textlink' href='./2023-09-25-dtail-usage-examples.html'>2023-09-25 DTail usage examples</a><br />
<a class='textlink' href='./2022-10-30-installing-dtail-on-openbsd.html'>2022-10-30 Installing DTail on OpenBSD</a><br />
<a class='textlink' href='./2022-03-06-the-release-of-dtail-4.0.0.html'>2022-03-06 The release of DTail 4.0.0</a><br />
<a class='textlink' href='./2021-04-22-dtail-the-distributed-log-tail-program.html'>2021-04-22 DTail - The distributed log tail program (You are currently reading this)</a><br />
<br />
<a class='textlink' href='../'>Back to the main site</a><br />
<p class="footer">
Generated with <a href="https://codeberg.org/snonux/gemtexter">Gemtexter 3.0.1-develop</a> |
served by <a href="https://www.OpenBSD.org">OpenBSD</a>/<a href="https://man.openbsd.org/relayd.8">relayd(8)</a>+<a href="https://man.openbsd.org/httpd.8">httpd(8)</a> |
<a href="https://foo.zone/site-mirrors.html">Site Mirrors</a>
</p>
</body>
</html>
