<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Unveiling `guprecords.raku`: Global Uptime Records with Raku</title>
<link rel="shortcut icon" type="image/gif" href="/favicon.ico" />
<link rel="stylesheet" href="../style.css" />
<link rel="stylesheet" href="style-override.css" />
</head>
<body>
<p class="header">
<a href="https://foo.zone">Home</a> | <a href="https://codeberg.org/snonux/foo.zone/src/branch/content-md/gemfeed/2023-05-01-unveiling-guprecords:-uptime-records-with-raku.md">Markdown</a> | <a href="gemini://foo.zone/gemfeed/2023-05-01-unveiling-guprecords:-uptime-records-with-raku.gmi">Gemini</a>
</p>
<h1 style='display: inline' id='unveiling-guprecordsraku-global-uptime-records-with-raku'>Unveiling <span class='inlinecode'>guprecords.raku</span>: Global Uptime Records with Raku</h1><br />
<br />
<span class='quote'>Published at 2023-04-30T13:10:26+03:00</span><br />
<br />
<pre>
+-----+-----------------+-----------------------------+
| Pos |            Host |                    Lifespan |
+-----+-----------------+-----------------------------+
|  1. |        dionysus |  8 years, 6 months, 17 days |
|  2. |          uranus |  7 years, 2 months, 16 days |
|  3. |   alphacentauri |  6 years, 9 months, 13 days |
|  4. |         *vulcan |   4 years, 5 months, 6 days |
|  5. |             sun |  3 years, 10 months, 2 days |
|  6. |           uugrn |   3 years, 5 months, 5 days |
|  7. |       deltavega |  3 years, 1 months, 21 days |
|  8. |           pluto | 2 years, 10 months, 30 days |
|  9. |         tauceti |  2 years, 3 months, 22 days |
| 10. |        callisto |  2 years, 3 months, 13 days |
+-----+-----------------+-----------------------------+
</pre>
<br />
<h2 style='display: inline' id='table-of-contents'>Table of Contents</h2><br />
<br />
<ul>
<li><a href='#unveiling-guprecordsraku-global-uptime-records-with-raku'>Unveiling <span class='inlinecode'>guprecords.raku</span>: Global Uptime Records with Raku</a></li>
<li>⇢ <a href='#introduction'>Introduction</a></li>
<li>⇢ <a href='#how-guprecords-works'>How Guprecords works</a></li>
<li>⇢ <a href='#example'>Example</a></li>
<li>⇢ <a href='#conclusion'>Conclusion</a></li>
</ul><br />
<h2 style='display: inline' id='introduction'>Introduction</h2><br />
<br />
<span>For fun, I am tracking the uptime of various personal machines (servers, laptops, workstations...). I have been doing this for over ten years now, so I have a lot of statistics collected.</span><br />
<br />
<span>As a result of this, I am introducing <span class='inlinecode'>guprecords.raku</span>, a handy Raku script that helps me combine uptime statistics from multiple servers into one comprehensive report. In this blog post, I&#39;ll explore what Guprecords is and some examples of its application. I will also add some notes on Raku.</span><br />
<br />
<span>Guprecords, or global uptime records, is a Raku script designed to generate a consolidated uptime report from multiple hosts:</span><br />
<br />
<a class='textlink' href='https://codeberg.org/snonux/guprecords'>https://codeberg.org/snonux/guprecords</a><br />
<a class='textlink' href='https://raku.org'>The Raku Programming Language</a><br />
<br />
<span>A previous version of Guprecords was actually written in Perl, the older and more established language from which Raku was developed. One of the primary motivations for rewriting Guprecords in Raku was to learn the language and explore its features. Raku is a more modern and powerful language compared to Perl, and working on a real-world project like Guprecords provided a practical and engaging way to learn the language.</span><br />
<br />
<span>Over the last years, I have been reading the following books and resources about Raku:</span><br />
<br />
<ul>
<li>Raku Guide (at raku.guide)</li>
<li>Think Perl 6</li>
<li>Raku Fundamentals</li>
<li>Raku Recipes</li>
</ul><br />
<span>And I have been following the Raku newsletter, and sometimes I have been lurking around in the IRC channels, too. Watching Raku coding challenges on YouTube was pretty fun, too. However, nothing beats actually using Raku to learn the language. After reading all of these resources, I may have a good idea about the features and paradigms, but I am by far not an expert.</span><br />
<br />
<h2 style='display: inline' id='how-guprecords-works'>How Guprecords works</h2><br />
<br />
<span>Guprecords works in three stages:</span><br />
<br />
<ul>
<li>1. Generating uptime statistics using <span class='inlinecode'>uptimed</span>: First, I need to install and run <span class='inlinecode'>uptimed</span> on each host to generate uptime statistics. This tool is available for most common Linux and *BSD distributions and macOS via Homebrew.</li>
<li>2. Collecting uptime records to a central location: The next step involves collecting the raw uptime statistics files generated by <span class='inlinecode'>uptimed</span> on each host. It&#39;s a good idea to store all record files in a central git repository. The records file contains information about the total uptime since boot, boot time, and the operating system and kernel version. Guprecords itself does not do the collection part, but have a look at the <span class='inlinecode'>README.md</span> in the git repository for some guidance.</li>
<li>3. Generating global uptime stats: Finally, run the <span class='inlinecode'>guprecords.raku</span> script with the appropriate flags to create a global uptime report. For example, I can use the following command:</li>
</ul><br />
<!-- Generator: GNU source-highlight 3.1.9
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre>$ raku guprecords.raku --stats=dir=$HOME/git/uprecords/stats --all
</pre>
<br />
<span>This command will generate a comprehensive uptime report from the collected statistics, making it easy to review and enjoy the data.</span><br />
<br />
<span>Guprecords supports the following features:</span><br />
<br />
<ul>
<li>Supports multiple categories: Host, Kernel, KernelMajor, and KernelName</li>
<li>Supports multiple metrics: Boots, Uptime, Score, Downtime, and Lifespan</li>
<li>Output formats available: Plaintext, Markdown, and Gemtext</li>
<li>Provides top entries based on the specified limit</li>
</ul><br />
<h2 style='display: inline' id='example'>Example</h2><br />
<br />
<span>You have already seen an example at the very top of this post, where the hosts were grouped by their total lifespans (uptime+downtime). Here&#39;s an example of what the global uptime report (grouped by total host uptimes) might look like:</span><br />
<br />
<pre>
Top 20 Uptime&#39;s by Host

+-----+-----------------+-----------------------------+
| Pos |            Host |                      Uptime |
+-----+-----------------+-----------------------------+
|  1. |         *vulcan |   4 years, 5 months, 6 days |
|  2. |          uranus | 3 years, 11 months, 21 days |
|  3. |             sun |  3 years, 9 months, 26 days |
|  4. |           uugrn |   3 years, 5 months, 5 days |
|  5. |       deltavega |  3 years, 1 months, 21 days |
|  6. |           pluto | 2 years, 10 months, 29 days |
|  7. |         tauceti |  2 years, 3 months, 19 days |
|  8. |       tauceti-f |  1 years, 9 months, 18 days |
|  9. | *ultramega15289 |  1 years, 8 months, 17 days |
| 10. |          *earth |  1 years, 5 months, 22 days |
| 11. |       *blowfish |  1 years, 4 months, 20 days |
| 12. |   ultramega8477 |  1 years, 3 months, 25 days |
| 13. |           host0 |   1 years, 3 months, 9 days |
| 14. |       tauceti-e |  1 years, 2 months, 20 days |
| 15. |        makemake |   1 years, 1 months, 6 days |
| 16. |        callisto | 0 years, 10 months, 31 days |
| 17. |   alphacentauri | 0 years, 10 months, 28 days |
| 18. |          london |  0 years, 9 months, 16 days |
| 19. |         twofish |  0 years, 8 months, 31 days |
| 20. |     *fishfinger |  0 years, 8 months, 17 days |
+-----+-----------------+-----------------------------+
</pre>
<br />
<span>This table ranks the top 20 hosts based on their total uptime, with the host having the highest uptime at the top. The hosts marked with <span class='inlinecode'>*</span> are still active, means stats were collected within the last couple of months. </span><br />
<br />
<span>My up to date stats can be seen here:</span><br />
<br />
<a class='textlink' href='../uptime-stats.html'>My machine uptime stats</a><br />
<br />
<span>Just recently, I decommissioned <span class='inlinecode'>vulcan</span> (the number one stop from above), which used to be my CentOS 7 (initially CentOS 6) VM hosting my personal NextCloud and Wallabag (which I modernised just recently with a brand new shiny Rocky Linux 9 VM). This was the last <span class='inlinecode'>uptimed</span> output before shutting it down (it always makes me feel sentimental decommissioning one of my machines <span class='inlinecode'>:&#39;-(</span>):</span><br />
<br />
<pre>
     #               Uptime | System                                     Boot up
----------------------------+---------------------------------------------------
     1   545 days, 17:58:15 | Linux 3.10.0-1160.15.2.e  Sun Jul 25 19:32:25 2021
     2   279 days, 10:12:14 | Linux 3.10.0-957.21.3.el  Sun Jun 30 12:43:41 2019
     3   161 days, 06:08:43 | Linux 3.10.0-1160.15.2.e  Sun Feb 14 11:05:38 2021
     4   107 days, 01:26:35 | Linux 3.10.0-957.1.3.el7  Thu Dec 20 09:29:13 2018
     5    96 days, 21:13:49 | Linux 3.10.0-1127.13.1.e  Sat Jul 25 17:56:22 2020
-&gt;   6    89 days, 23:05:32 | Linux 3.10.0-1160.81.1.e  Sun Jan 22 12:39:36 2023
     7    63 days, 18:30:45 | Linux 3.10.0-957.10.1.el  Sat Apr 27 18:12:43 2019
     8    63 days, 06:53:33 | Linux 3.10.0-1127.8.2.el  Sat May 23 10:41:08 2020
     9    48 days, 11:44:49 | Linux 3.10.0-1062.18.1.e  Sat Apr  4 22:56:07 2020
    10    42 days, 08:00:13 | Linux 3.10.0-1127.19.1.e  Sat Nov  7 11:47:33 2020
    11    36 days, 22:57:19 | Linux 3.10.0-1160.6.1.el  Sat Dec 19 19:47:57 2020
    12    21 days, 06:16:28 | Linux 3.10.0-957.10.1.el  Sat Apr  6 11:56:01 2019
    13    12 days, 20:11:53 | Linux 3.10.0-1160.11.1.e  Mon Jan 25 18:45:27 2021
    14     7 days, 21:29:18 | Linux 3.10.0-1127.13.1.e  Fri Oct 30 14:18:04 2020
    15     6 days, 20:07:18 | Linux 3.10.0-1160.15.2.e  Sun Feb  7 14:57:35 2021
    16     1 day , 21:46:41 | Linux 3.10.0-957.1.3.el7  Tue Dec 18 11:42:19 2018
    17     0 days, 01:25:57 | Linux 3.10.0-957.1.3.el7  Tue Dec 18 10:16:08 2018
    18     0 days, 00:42:34 | Linux 3.10.0-1160.15.2.e  Sun Jul 25 18:49:38 2021
    19     0 days, 00:08:32 | Linux 3.10.0-1160.81.1.e  Sun Jan 22 12:30:52 2023
----------------------------+---------------------------------------------------
1up in     6 days, 22:08:18 | at                        Sat Apr 29 10:53:25 2023
no1 in   455 days, 18:52:44 | at                        Sun Jul 21 07:37:51 2024
    up  1586 days, 00:20:28 | since                     Tue Dec 18 10:16:08 2018
  down     0 days, 01:08:32 | since                     Tue Dec 18 10:16:08 2018
   %up               99.997 | since                     Tue Dec 18 10:16:08 2018
</pre>
<br />
<h2 style='display: inline' id='conclusion'>Conclusion</h2><br />
<br />
<span>Guprecords is a small, yet powerful tool for analyzing uptime statistics. While developing Guprecords, I have come to truly appreciate and love Raku&#39;s expressiveness. The language is designed to be both powerful and flexible, allowing developers to express their intentions and logic more clearly and concisely.</span><br />
<br />
<span>Raku&#39;s expressive syntax, support for multiple programming paradigms, and unique features, such as grammars and lazy evaluation, make it a joy to work with. </span><br />
<br />
<span>Working on Guprecords in Raku has been an enjoyable experience, and I&#39;ve found that Raku&#39;s expressiveness has significantly contributed to the overall quality and effectiveness of the script. The language&#39;s ability to elegantly express complex logic and data manipulation tasks makes it an excellent choice for developing tools like these, where expressiveness and productiveness are of the utmost importance.</span><br />
<br />
<span>So far, I have only scratched the surface of what Raku can do. I hope to find more time to become a regular Rakoon (a Raku Programmer). I have many Ideas for other small tools like Guprecords, but the challenge is finding the time. I&#39;d love to explore Raku Grammars and also I would love to explore writing concurrent code in Raku (I also love Go (Golang), btw!). Ideas for future Raku personal projects include:</span><br />
<br />
<ul>
<li>A log file analyzer, for generating anonymized <span class='inlinecode'>foo.zone</span> visitor stats for both, the Web and Gemini.</li>
<li>A social media sharing scheduler a la <span class='inlinecode'>buffer.com</span>. I am using Buffer at the moment to share posts on Mastadon, Twitter, Telegram and LinkedIn, but it is proprietary and also it&#39;s not really reliable.</li>
<li>Rewrite the static photo album generator of <span class='inlinecode'>irregular.ninja</span> in Raku (from Bash).</li>
</ul><br />
<span>E-Mail your comments to hi@foo.zone :-)</span><br />
<br />
<span>Other related posts are:</span><br />
<br />
<a class='textlink' href='./2023-05-01-unveiling-guprecords:-uptime-records-with-raku.html'>2023-05-01 Unveiling <span class='inlinecode'>guprecords.raku</span>: Global Uptime Records with Raku (You are currently reading this)</a><br />
<a class='textlink' href='./2022-06-15-sweating-the-small-stuff.html'>2022-06-15 Sweating the small stuff - Tiny projects of mine</a><br />
<a class='textlink' href='./2022-05-27-perl-is-still-a-great-choice.html'>2022-05-27 Perl is still a great choice</a><br />
<a class='textlink' href='./2011-05-07-perl-daemon-service-framework.html'>2011-05-07 Perl Daemon (Service Framework)</a><br />
<a class='textlink' href='./2008-06-26-perl-poetry.html'>2008-06-26 Perl Poetry</a><br />
<br />
<a class='textlink' href='../'>Back to the main site</a><br />
<p class="footer">
Generated with <a href="https://codeberg.org/snonux/gemtexter">Gemtexter 3.0.1-develop</a> |
served by <a href="https://www.OpenBSD.org">OpenBSD</a>/<a href="https://man.openbsd.org/relayd.8">relayd(8)</a>+<a href="https://man.openbsd.org/httpd.8">httpd(8)</a> |
<a href="https://foo.zone/site-mirrors.html">Site Mirrors</a>
</p>
</body>
</html>
